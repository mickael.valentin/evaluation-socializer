import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
//import {SigninComponent} from '../../pages/signin/signin.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  myRouterUrl: any;
  hideHeader: boolean = true;

  constructor(private router: Router) { 
    //console.log(_router);
    //this.router = _router.url; 
    //console.log(this.router);
    //this.signinComponent.showNav = true;
  }

  ngOnInit() {
    this.myRouterUrl = this.router;
    console.log(this.myRouterUrl);
    if(this.router.url == '/log-in' || this.router.url == '/sign-in'){
      this.hideHeader = false;
    }
  }

}
