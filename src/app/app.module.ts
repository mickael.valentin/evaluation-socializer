import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { SigninComponent } from './pages/signin/signin.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { DashboardMonitoringComponent } from './pages/dashboard-monitoring/dashboard-monitoring.component';
import { DashboardPreferencesComponent } from './pages/dashboard-preferences/dashboard-preferences.component';
import { HeaderLoginComponent } from './layout/header-login/header-login.component';
import { HeaderSigninComponent } from './layout/header-signin/header-signin.component';
import { UpdateUserComponent } from './pages/update-user/update-user.component';
import { UpdateUserPasswordComponent } from './pages/update-user-password/update-user-password.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SigninComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    DashboardMonitoringComponent,
    DashboardPreferencesComponent,
    HeaderLoginComponent,
    HeaderSigninComponent,
    UpdateUserComponent,
    UpdateUserPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
