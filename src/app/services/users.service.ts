import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DashboardPreferencesComponent} from '../pages/dashboard-preferences/dashboard-preferences.component';
import {User} from '../models/user.model';


const token = 'EAAFvno2ZCt60BAIqxpQdacDkFZAXV1MviywfopeECzkZC2PbNrvo5dDjxZApbokycrnENjREw3gRSKDOWTqZA9M9uwtf1AXND9UJmx4fSZAMMerFfO0CE9E0pB5kpdbCzzHbCFZAShZCeERABngNNQvdlCsqHMMCc9tYys9nyoWrJERvuyWRyGPliBIodQG25e6UenvC1jAjcQZDZD';
const appId='404201750378413';
const appSecret='69e183277391fcb69bdcb804d8cd1c9e';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json'
    })
  }

  constructor(private http:HttpClient) { }

  getMyAccountFacebook(){
    return this.http.get('https://graph.facebook.com/me?fields=id,name,birthday,email,hometown,friends,likes,posts&access_token='+localStorage.getItem("FB_long_acces_token"));
  }

  getLongLivedAccessToken(){
    return this.http.get('https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id='+appId+'&client_secret='+appSecret+'&fb_exchange_token='+token);

  }

  showOneUser(id){
    return this.http.get('http://localhost:3000/api/users/'+id);
  }

  updateUser(id, user: User){
    return this.http.put('http://localhost:3000/api/users/'+id, user, this.httpOptions);    
  }

  updateUserPassword(id, user: User){
    return this.http.put('http://localhost:3000/api/users/mdp/'+id, user, this.httpOptions);    
  }
}
