import { Injectable } from '@angular/core';
import {User} from '../models/user.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router, Routes } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json'
    })
  }

  constructor(private httpClient: HttpClient, private router:Router) { }

  login(user: User){
    return this.httpClient.post('http://localhost:3000/api/auth/login', user, this.httpOptions);
    

  }

  register(user: User){
    return this.httpClient.post('http://localhost:3000/api/auth/register', user, this.httpOptions);
  }

  logout(){
    return this.httpClient.get('http://localhost:3000/api/auth/logout');
    /*localStorage.removeItem('currentUser');
    this.router.navigate(['log-in']);*/

  }

  islogged(){

  }
}
