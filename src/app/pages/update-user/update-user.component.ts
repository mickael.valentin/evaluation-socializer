import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, Routes } from '@angular/router';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  updateForm : FormGroup;
  submitted: boolean = false;
  formValue:any;
  user:any;

  emailError: boolean = false;

  constructor(private formBuilder: FormBuilder, private usersService: UsersService, private router:Router) { }

  ngOnInit() {

    this.updateForm = this.formBuilder.group({  
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],
      number: ['', [Validators.required]],
      
    })
  }

  get form(){
    return this.updateForm.controls;
  }

  onSubmit(){
    console.log(this.updateForm);
    //if(this.registerForm.controls.email.errors.required == true){
      //this.emailError = true;
    this.submitted = true;

    if(!this.updateForm.invalid){
      this.formValue = JSON.stringify(this.updateForm.value);
      this.usersService.updateUser(sessionStorage.getItem("currentUser"), this.formValue).subscribe(
        (data) => {
          this.user = data;
          console.log(data);
          this.router.navigate(['dashboard']);
        },
        (error) =>{
          console.log(error);
        }
      )
    }
  }

}
