import { Component, OnInit } from '@angular/core';
import{UsersService} from '../../services/users.service';
//import{DashboardComponent} from '../dashboard/dashboard.component';
import {AuthService} from '../../services/auth.service';
import { Router, Routes } from '@angular/router';

let FB;

@Component({
  selector: 'app-dashboard-preferences',
  templateUrl: './dashboard-preferences.component.html',
  styleUrls: ['./dashboard-preferences.component.scss']
})
export class DashboardPreferencesComponent implements OnInit {

  LongLivedAccessToken:any;

  constructor(private UsersService:UsersService, private authService: AuthService, private router:Router) { }

  ngOnInit() {

    (window as any).fbAsyncInit = function () {
      FB.init({
        appId: '404201750378413',
        cookie: true,
        xfbml: true,
        version: 'v3.2'
      });

      FB.AppEvents.logPageView();

    };

    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


  }

  submitLogin() {
    console.log("submit login to facebook");
    // FB.login();
    FB.login((response) => {
      console.log('submitLogin', response);
      if (response.authResponse) {
        this.UsersService.getLongLivedAccessToken().subscribe(data => {
          this.LongLivedAccessToken = data;
          console.log(this.LongLivedAccessToken);
          //this.dashboardcomponent.getMyAccountFacebook();
          localStorage.setItem('FB_long_acces_token', this.LongLivedAccessToken.access_token);

        })
        //login success
        //login success code here
        //redirect to home page
      }
      else {
        console.log('User login failed');
      }
    });
  }

  Deconnexion(){
    localStorage.removeItem('currentUser');   
    sessionStorage.removeItem('currentUser');     
    this.authService.logout();
    this.router.navigate(['log-in']);
  }

}
