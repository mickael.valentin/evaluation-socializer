import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../services/auth.service';
import { Router, Routes } from '@angular/router';


@Component({
  selector: 'app-dashboard-monitoring',
  templateUrl: './dashboard-monitoring.component.html',
  styleUrls: ['./dashboard-monitoring.component.scss']
})
export class DashboardMonitoringComponent implements OnInit {

  facebookusers:any;


  constructor(private UsersService:UsersService, private authService: AuthService, private router:Router) { }

  ngOnInit() {

    this.UsersService.getMyAccountFacebook().subscribe(data => {
      this.facebookusers = [data];
      console.log(this.facebookusers);
    })
  }

  Deconnexion(){
    localStorage.removeItem('currentUser');  
    sessionStorage.removeItem('currentUser');      
    this.authService.logout();
    this.router.navigate(['log-in']);
  }

}
