import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router, Routes } from '@angular/router';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-update-user-password',
  templateUrl: './update-user-password.component.html',
  styleUrls: ['./update-user-password.component.scss']
})
export class UpdateUserPasswordComponent implements OnInit {

  updateForm : FormGroup;
  submitted: boolean = false;
  formValue:any;
  password:any;

  constructor(private formBuilder: FormBuilder, private usersService: UsersService, private router:Router) { }

  ngOnInit() {

    this.updateForm = this.formBuilder.group({  
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  get form(){
    return this.updateForm.controls;
  }

  onSubmit(){
    console.log(this.updateForm);
    //if(this.registerForm.controls.email.errors.required == true){
      //this.emailError = true;
    this.submitted = true;

    if(!this.updateForm.invalid){
      this.formValue = JSON.stringify(this.updateForm.value);
      this.usersService.updateUserPassword(sessionStorage.getItem("currentUser"), this.formValue).subscribe(
        (data) => {
          this.password = data;
          console.log(data);
          this.router.navigate(['dashboard']);
        },
        (error) =>{
          console.log(error);
        }
      )
    }
  }

}
