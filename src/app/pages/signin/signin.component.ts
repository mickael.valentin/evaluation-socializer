import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import { Router, Routes } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  registerForm : FormGroup;
  submitted: boolean = false;
  formValue:any;

  user:any;
  payload:any;

  emailError: boolean = false;
  //showNav:boolean;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router:Router) {
    //this.showNav = false;
   }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({  
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      username: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(6)]],
      
    })
  }

  get form(){
    return this.registerForm.controls;
  }

  onSubmit(){
    console.log(this.registerForm);
    //if(this.registerForm.controls.email.errors.required == true){
      //this.emailError = true;
    this.submitted = true;

    if(!this.registerForm.invalid){
      this.formValue = JSON.stringify(this.registerForm.value);
      this.authService.register(this.formValue).subscribe(
        (data) => {
          this.user = data;
          console.log(data);
          if(this.user && this.user.token){
            this.payload = window.atob(this.user.token.split('.')[1]);
            console.log(this.payload);
            localStorage.setItem('currentUser', this.user.token);
            //sessionStorage.setItem('currentUser', this.user);            
            this.router.navigate(['log-in']);
          }
        },
        (error) =>{
          console.log(error);
        }
      )
    }
  }
}
