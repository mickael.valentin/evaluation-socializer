import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../services/auth.service';
import { Router, Routes } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  facebookusers:any;
  users:any;

  constructor(private UsersService:UsersService, private authService: AuthService, private router:Router) { }

  ngOnInit() {

    this.getUser();
    
  }

  getMyAccountFacebook(){
    this.UsersService.getMyAccountFacebook().subscribe(data => {
      this.facebookusers = [data];
      console.log(this.facebookusers);
    })
  }

  getUser(){
    this.UsersService.showOneUser(sessionStorage.getItem("currentUser")).subscribe(data => {
      this.users = [data];
      console.log(this.users);
    })
  }

  Deconnexion(){
    localStorage.removeItem('currentUser'); 
    sessionStorage.removeItem('currentUser');           
    this.authService.logout();
    this.router.navigate(['log-in']);
  }

}
