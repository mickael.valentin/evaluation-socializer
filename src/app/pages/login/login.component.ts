import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import { Router, Routes } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
  submitted: boolean = false;
  formValue:any;

  user:any;
  payload:any;


  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router:Router) { }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({  
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  onSubmit(){
    console.log(this.loginForm);

    if(!this.loginForm.invalid){
      this.formValue = JSON.stringify(this.loginForm.value);
      this.authService.login(this.formValue).subscribe(
        (data) => {
          this.user = data;
          console.log(data);
          if(this.user && this.user.token){
            this.payload = window.atob(this.user.token.split('.')[1]);
            console.log(this.payload);
            localStorage.setItem('currentUser', this.user.token);
            sessionStorage.setItem('currentUser', this.user.id);            
            this.router.navigate(['dashboard']);
            console.log(this.user);
          }
        },
        (error) =>{
          console.log(error);
        }
      )
    }
  }

}
