import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './pages/home/home.component';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {DashboardMonitoringComponent} from './pages/dashboard-monitoring/dashboard-monitoring.component';
import {DashboardPreferencesComponent} from './pages/dashboard-preferences/dashboard-preferences.component';
import {SigninComponent} from './pages/signin/signin.component';
import {LoginComponent} from './pages/login/login.component';
import {UpdateUserComponent} from './pages/update-user/update-user.component';
import {UpdateUserPasswordComponent} from './pages/update-user-password/update-user-password.component';
import {AuthGuard} from './guards/auth.guards';



const routes: Routes = [

  {
    path:"",
    component:HomeComponent
  },
  {
    path:"dashboard",
    component:DashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path:"dashboard-monitoring",
    component:DashboardMonitoringComponent,
    canActivate:[AuthGuard]
  },
  {
    path:"dashboard-preferences",
    component:DashboardPreferencesComponent,
    canActivate:[AuthGuard]
  },
  {
    path:"log-in",
    component:LoginComponent
  },
  {
    path:"sign-in",
    component:SigninComponent
  },
  {
    path:"user/update",
    component:UpdateUserComponent,
    canActivate:[AuthGuard]    
  },
  {
    path:"user/update/mdp",
    component:UpdateUserPasswordComponent,
    canActivate:[AuthGuard]    
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
